import { createContext, Dispatch, SetStateAction } from "react";

interface IAlertContext {
  alert: {
    title?: string;
    message: string;
  } | null;
  setAlert: Dispatch<SetStateAction<any>> | any;
}

export const AlertContext = createContext({
  alert: {},
  setAlert: null,
} as IAlertContext);
