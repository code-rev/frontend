import { createContext, Dispatch, SetStateAction } from "react";

interface IUserContext {
  user: any | null;
  setUser: Dispatch<SetStateAction<any>> | any;
}

export const UserContext = createContext({
  user: null,
  setUser: null,
} as IUserContext);
