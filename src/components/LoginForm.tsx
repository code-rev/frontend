import React, { useState, useContext } from "react";
import { gql, useMutation } from "@apollo/client";
import { UserContext } from "../context/UserContext";
import { Link } from "react-router-dom";
import routes from "../router/routes";
import { PersistentAuth, storeUser } from "../plugins/auth";

export default function LoginForm() {
  const { setUser } = useContext(UserContext);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const query = gql`
    mutation Login($username: String!, $password: String!) {
      login(username: $username, password: $password) {
        accessToken
        refreshToken
      }
    }
  `;
  const [login, { loading, error, data }] = useMutation(query);
  const validate = () => {
    if (!username || !password) return false;
    return true;
  };

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    login({ variables: { username, password } });
  };

  if (loading) return <>"Loading..."</>;
  if (error) return <>"Error!"</>;
  if (data) {
    if (data.login) {
      PersistentAuth.accessToken = data.login.accessToken;
      PersistentAuth.refreshToken = data.login.refreshToken;
      storeUser(setUser);
    }
  }

  return (
    <form onSubmit={onSubmit}>
      <input
        className="linear-input"
        type="text"
        value={username}
        placeholder="Username"
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setUsername(event.target.value)
        }
        autoFocus
      />
      <input
        className="linear-input"
        type="password"
        value={password}
        placeholder="Password"
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setPassword(event.target.value)
        }
      />
      <div className="flex justify-end">
        <button type="submit" disabled={!validate()} className="btn">
          Login
        </button>
      </div>
      <div className="text-center">
        <Link to={routes.register.path}>Don't have an account!?</Link>
      </div>
    </form>
  );
}
