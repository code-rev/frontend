import React from "react";
import { ICode } from "./CodeList";
import { Link } from "react-router-dom";
import routes from "../router/routes";

interface IProps {
  code: ICode;
}

export default function CodeListItem({ code }: IProps) {
  const codeUrl = () => {
    return routes.code.path.replace(":id", String(code.post.id));
  };
  return (
    <div className="flex rounded-sm bg-gray-100 mb-1 hover:bg-yellow-100">
      <div className="flex-col p-3">{code.post.rate}</div>
      <div className="flex-col">
        <h3 className="text-xl inline-block mr-1">
          <Link to={codeUrl()}>{code.title}</Link>
        </h3>
        <div className="bg-red-400 inline-block text-red-100 px-1 rounded-sm">
          {code.language.title}
        </div>
        <div className="block">
          Posted @<span className="text-gray-700">{code.post.createdAt}</span>{" "}
          By <span className="text-gray-700">{code.post.author.username}</span>
        </div>
      </div>
    </div>
  );
}
