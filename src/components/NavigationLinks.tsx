import React, { useContext } from "react";
import { Link } from "react-router-dom";
import routes from "../router/routes";
import { UserContext } from "../context/UserContext";

interface IProps {
  className?: string;
  itemClassName?: string;
}

interface Items {
  name: string;
  to: string;
}

export default function NavigationLinks(props: IProps) {
  const { user } = useContext(UserContext);
  const items: Items[] = [
    {
      name: "Home",
      to: routes.home.path,
    },
    {
      name: "Languages",
      to: routes.languages.path,
    },
  ];

  if (user) {
    items.push(
      ...[
        {
          name: "Dashboard",
          to: routes.dashboard.path,
        },
        {
          name: "Logout",
          to: routes.logout.path,
        },
      ]
    );
  } else {
    items.push(
      ...[
        {
          name: "Login / Register",
          to: routes.login.path,
        },
      ]
    );
  }
  return (
    <ul className={props.className}>
      {items.map((item) => (
        <Link to={item.to} key={item.name} className={props.itemClassName}>
          <li>{item.name}</li>
        </Link>
      ))}
    </ul>
  );
}
