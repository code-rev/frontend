import React, { useState } from "react";
import { gql, useMutation } from "@apollo/client";
import { Redirect, Link } from "react-router-dom";
import routes from "../router/routes";

export default function RegisterForm() {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const validate = () => {
    if (
      email === "" ||
      username === "" ||
      password === "" ||
      password !== confirmPassword
    )
      return false;

    return true;
  };
  const mutation = gql`
    mutation Register($username: String!, $password: String!, $email: String!) {
      registerUser(username: $username, password: $password, email: $email) {
        id
      }
    }
  `;
  const [register, { loading, error, data }] = useMutation(mutation);
  const onSubmit = async () => {
    await register({
      variables: {
        username,
        password,
        email,
      },
    });
  };

  if (loading) return <>"Loading..."</>;
  if (error) return <>"Error!"</>;
  if (data) {
    if (data.registerUser) {
      return <Redirect to={routes.login.path} />;
    }
  }

  return (
    <form onSubmit={onSubmit}>
      <input
        className="linear-input"
        type="email"
        placeholder="Email: john@doe.com"
        value={email}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setEmail(event.target.value)
        }
        autoFocus
      />
      <input
        className="linear-input"
        type="text"
        placeholder="John_Doe"
        value={username}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setUsername(event.target.value)
        }
      />
      <input
        className="linear-input"
        type="password"
        placeholder="Password"
        value={password}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setPassword(event.target.value)
        }
      />
      <input
        className="linear-input"
        type="password"
        placeholder="Confirm Password"
        value={confirmPassword}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setConfirmPassword(event.target.value)
        }
      />
      <div className="flex justify-end">
        <button type="submit" className="btn" disabled={!validate()}>
          Register!
        </button>
      </div>
      <div className="text-center">
        <Link to={routes.login.path}>Already have an account!?</Link>
      </div>
    </form>
  );
}
