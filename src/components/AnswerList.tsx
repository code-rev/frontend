import React from "react";
import PostItem, { IPost } from "./PostItem";

interface IProps {
  answers: IPost[] | undefined;
}

export default function AnswerList(props: IProps) {
  if (!props.answers) return <></>;
  const answers = props.answers.map((answer) => <PostItem key={answer.id} post={answer} />);
  return <>{answers}</>;
}
