import React, { useState, useEffect } from "react";
import ReactMarkdown from "react-markdown";
import { gql } from "@apollo/client";
import apolloClient from "../api/graphql";
import { Redirect } from "react-router-dom";
import routes from "../router/routes";

export function CodeForm() {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [languageId, setLanguageId] = useState<string | number>("");
  const [languages, setLanguages] = useState<{ id: number; title: string }[]>(
    []
  );
  const [postId, setPostId] = useState<number | null>(null);
  useEffect(() => {
    const languageQuery = gql`
      {
        getLanguages {
          id
          title
        }
      }
    `;
    apolloClient
      .query({ query: languageQuery })
      .then(({ data }) => setLanguages(data.getLanguages));
  });
  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const mutation = gql`
      mutation CreateCode(
        $title: String!
        $languageId: Float!
        $content: String!
      ) {
        createCode(title: $title, languageId: $languageId, content: $content) {
          post {
            id
          }
        }
      }
    `;
    const { data } = await apolloClient.mutate({
      mutation,
      variables: {
        title,
        content,
        languageId,
      },
    });
    setPostId(data.createCode.post.id);
  };

  const validate = () => {
    if (!languageId || !title || !content) return false;
    return true;
  };

  if (postId !== null) {
    const codeUrl = () => {
      return routes.code.path.replace(":id", String(postId));
    };
    return <Redirect to={codeUrl()} />;
  }
  return (
    <form onSubmit={onSubmit}>
      <input
        className="linear-input"
        type="text"
        placeholder="Title"
        value={title}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setTitle(event.target.value)
        }
        autoFocus
      />
      <select
        value={languageId}
        onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
          setLanguageId(Number(event.target.value))
        }
      >
        <option value="" disabled>
          Select Language
        </option>
        {languages.map((language) => (
          <option key={language.id} value={language.id}>
            {language.title}
          </option>
        ))}
      </select>
      <textarea
        value={content}
        placeholder="Content..."
        onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) =>
          setContent(event.target.value)
        }
        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 mb-2"
      ></textarea>
      <div>
        <h3 className="text-2xl">Preview</h3>
        <ReactMarkdown source={content} />
      </div>
      <div>
        <button className="btn" disabled={!validate()}>
          Create Code!
        </button>
      </div>
    </form>
  );
}
