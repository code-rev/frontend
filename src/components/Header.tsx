import React from "react";
import NavigationLinks from "./NavigationLinks";

export default function Header() {
  return (
    <header className="bg-gray-800 text-gray-200 fill-current px-3 mb-4">
      <div className="flex mx-auto justify-between items-center container">
        <div className="text-2xl">Code Rev</div>
        <div>
          <button className="block sm:hidden">
            <svg viewBox="0 0 100 80" width="40" height="40">
              <rect width="100" height="20"></rect>
              <rect y="30" width="100" height="20"></rect>
              <rect y="60" width="100" height="20"></rect>
            </svg>
          </button>
          <div className="hidden sm:block">
            <NavigationLinks
              className="flex"
              itemClassName="ml-6 hover:bg-pink-600 p-2"
            />
          </div>
        </div>
      </div>
    </header>
  );
}
