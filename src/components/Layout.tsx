import React, { ReactElement, useState } from "react";
import Header from "./Header";
import Alert from "./Alert";
import { AlertContext } from "../context/AlertContext";

interface IProps {
  children: ReactElement;
}

export default function Layout(props: IProps) {
  const [alert, setAlert] = useState(null);
  return (
    <AlertContext.Provider value={{ alert, setAlert }}>
        <Header />
        <main className="container mx-auto min-h-full">{props.children}</main>
        <Alert />
    </AlertContext.Provider>
  );
}
