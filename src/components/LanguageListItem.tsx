import React from "react";
import { ILanguage } from "./LanguageList";
import { Link } from "react-router-dom";
import routes from "../router/routes";

interface IProps {
  language: ILanguage;
}

export default function LanguageListItem({ language }: IProps) {
  const codesCountText = () => {
    if (language.codesCount > 1) return `${language.codesCount} codes posted`;
    if (language.codesCount === 1) return "1 code posted";
    return "No codes posted";
  };
  const languageUrl = () => {
    return routes.language.path.replace(":title", language.title.toLowerCase());
  };
  return (
    <div className="mb-1 p-3 rounded-sm bg-gray-100 hover:bg-yellow-100 sm:mx-2">
      <Link to={languageUrl()}>
        <div>
          <h3 className="text-xl inline-block mr-1">{language.title}</h3>
          <span className="bg-red-400 inline-block text-red-100 px-1 rounded-sm">
            {codesCountText()}
          </span>
        </div>
        <p>{language.description}</p>
      </Link>
    </div>
  );
}
