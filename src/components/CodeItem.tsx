import React from "react";
import PostItem, { IPost } from "./PostItem";
import AnswerList from "./AnswerList";
import AnswerForm from "./AnswerForm";

export interface ICode {
  title: string;
  post: IPost;
  language: {
    title: string;
  };
}

interface IProps {
  code: ICode;
  onNewAnswer: (newAnswer: IPost) => void;
}

export default function CodeItem({ code, onNewAnswer }: IProps) {
  return (
    <>
      <h1 className="text-3xl">{code.title}</h1>
      <PostItem post={code.post} />
      <AnswerList answers={code.post.answers} />
      <AnswerForm onAfterSubmit={onNewAnswer} codeId={code.post.id} />
    </>
  );
}
