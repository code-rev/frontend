import React from "react";

interface IProps {
  rate: number;
  onVoteUp: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onVoteDown: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

export default function Rating(props: IProps) {
  return (
    <div className="my-auto">
      <button className="m-0" onClick={props.onVoteUp}>
        <svg
          version="1.1"
          id="Capa_1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          height="20"
          width="20"
          viewBox="0 0 240.835 240.835"
        >
          <path
            id="Expand_Less"
            d="M129.007,57.819c-4.68-4.68-12.499-4.68-17.191,0L3.555,165.803c-4.74,4.74-4.74,12.427,0,17.155
c4.74,4.74,12.439,4.74,17.179,0l99.683-99.406l99.671,99.418c4.752,4.74,12.439,4.74,17.191,0c4.74-4.74,4.74-12.427,0-17.155
L129.007,57.819z"
          />
        </svg>
      </button>
      <div className="text-xl">{props.rate}</div>
      <button className="m-0" onClick={props.onVoteDown}>
        <svg
          version="1.1"
          id="Capa_1"
          xmlns="http://www.w3.org/2000/svg"
          width="20"
          height="20"
          viewBox="0 0 240.811 240.811"
        >
          <path
            id="Expand_More"
            d="M220.088,57.667l-99.671,99.695L20.746,57.655c-4.752-4.752-12.439-4.752-17.191,0
c-4.74,4.752-4.74,12.451,0,17.203l108.261,108.297l0,0l0,0c4.74,4.752,12.439,4.752,17.179,0L237.256,74.859
c4.74-4.752,4.74-12.463,0-17.215C232.528,52.915,224.828,52.915,220.088,57.667z"
          />
        </svg>
      </button>
    </div>
  );
}
