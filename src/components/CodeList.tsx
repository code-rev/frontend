import React from "react";
import CodeListItem from "./CodeListItem";

export interface ICode {
  title: string;
  post: {
    id: number;
    rate: number;
    createdAt: string;
    author: {
      username: string;
    };
  };
  language: {
    title: string;
  };
}

interface IProps {
  codes: ICode[];
}

export default function CodeList(props: IProps) {
  const codes = props.codes.map((code) => (
    <CodeListItem key={code.post.id} code={code} />
  ));
  return <div>{codes}</div>;
}
