import React from "react";
import LanguageListItem from "./LanguageListItem";

export interface ILanguage {
  id: number;
  title: string;
  description: string;
  codesCount: number;
}

interface IProps {
  languages: ILanguage[];
}

export default function LanguageList(props: IProps) {
  const languages = props.languages.map((language) => (
    <LanguageListItem key={language.id} language={language} />
  ));
  return <div className="grid grid-cols-1 md:grid-cols-2">{languages}</div>;
}
