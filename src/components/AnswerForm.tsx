import React, { useState, useContext } from "react";
import ReactMarkdown from "react-markdown";
import { gql } from "@apollo/client";
import apolloClient from "../api/graphql";
import { IPost } from "./PostItem";
import { UserContext } from "../context/UserContext";

interface IProps {
  onAfterSubmit: (newAnswer: IPost) => void;
  codeId: number;
}

export default function AnswerForm(props: IProps) {
  const [answer, setAnswer] = useState("");
  const { user } = useContext(UserContext);
  if (!user) return <></>;
  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const mutation = gql`
      mutation replyCode($replyToId: Float!, $content: String!) {
        replyCode(replyToId: $replyToId, content: $content) {
          id
          content
          createdAt
          updatedAt
          rate
          author {
            username
          }
        }
      }
    `;
    apolloClient
      .mutate({
        mutation,
        variables: {
          replyToId: props.codeId,
          content: answer,
        },
      })
      .then(({ data }) => {
        setAnswer("");
        return props.onAfterSubmit(data.replyCode);
      });
  };
  return (
    <div>
      <form onSubmit={onSubmit}>
        <textarea
          placeholder="What you think about this code?"
          value={answer}
          onChange={(event: React.ChangeEvent<HTMLTextAreaElement>) =>
            setAnswer(event.target.value)
          }
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 mb-2"
        ></textarea>
        {answer ? (
          <>
            <h3 className="text-xl">Preview</h3>
            <ReactMarkdown source={answer} />
          </>
        ) : (
          ""
        )}
        <div>
          <button
            type="submit"
            className="btn"
          >
            Post Answer!
          </button>
        </div>
      </form>
    </div>
  );
}
