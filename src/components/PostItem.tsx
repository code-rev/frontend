import React, { useState, useContext } from "react";
import ReactMarkdown from "react-markdown";
import Rating from "./Rating";
import { gql } from "@apollo/client";
import apolloClient from "../api/graphql";
import { UserContext } from "../context/UserContext";
import { AlertContext } from "../context/AlertContext";

export interface IPost {
  id: number;
  content: string;
  rate: number;
  createdAt: string;
  updatedAt: string;
  author: {
    username: string;
  };
  answers?: IPost[];
}

interface IProps {
  post: IPost;
}

export default function PostItem(props: IProps) {
  const [post, setPost] = useState(props.post);
  const { user } = useContext(UserContext);
  const { setAlert } = useContext(AlertContext);
  const vote = (vote: "+" | "-") => {
    if (!user)
      return setAlert({
        message: "You need to authorize yourself in order to vote!",
        title: "Access Denied!",
      });
    const mutation = gql`
      mutation voteUp($id: Float!, $vote: String!) {
        votePost(id: $id, vote: $vote) {
          id
          rate
        }
      }
    `;
    apolloClient
      .mutate({
        mutation,
        variables: {
          id: post.id,
          vote: vote,
        },
      })
      .then(({ data }) => {
        if (data.votePost) {
          const updatedPost = JSON.parse(JSON.stringify(post));
          if (vote === "+") updatedPost.rate++;
          else updatedPost.rate--;
          setPost(updatedPost);
        }
      });
  };

  return (
    <div className="grid grid-cols-12 w-full mb-32">
      <div className="col-start-1 col-end-2 text-center inline-grid">
        <Rating
          rate={post.rate}
          onVoteUp={() => vote("+")}
          onVoteDown={() => vote("-")}
        />
      </div>
      <div className="col-start-2 col-end-13 w-full relative">
        <div className="mb-5">
          <ReactMarkdown source={post.content} className="break-words" />
        </div>
        <div className="w-full md:flex md:justify-end">
          <div className="lg:w-1/3 md:w-1/2 bg-pink-900 text-white p-2 rounded-sm">
            <div>Posted at: {post.createdAt}</div>
            {post.createdAt !== post.updatedAt ? (
              <div>Editted at: {post.createdAt}</div>
            ) : (
              ""
            )}
            <div>By {post.author.username}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
