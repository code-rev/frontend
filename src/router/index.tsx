import React, { useContext } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "./routes";
import { UserContext } from "../context/UserContext";

export default function Router() {
  const { user } = useContext(UserContext);
  return (
    <Switch>
      {Object.keys(routes).map((name) => {
        const route = routes[name];
        let elementToRender = React.createElement(route.component, {});
        if (route.auth === true && !user) {
          elementToRender = <Redirect to={routes.login.path} />;
        } else if (route.auth === false && user) {
          elementToRender = <Redirect to={routes.dashboard.path} />;
        }
        return (
          <Route path={route.path} exact={route.exact} key={route.path}>
            {elementToRender}
          </Route>
        );
      })}
    </Switch>
  );
}
