import React from "react";
import Home from "../views/Home";
import Languages from "../views/Languages";
import Language from "../views/Language";
import Code from "../views/Code";
import Login from "../views/Login";
import Logout from "../views/Logout";
import Register from "../views/Register";
import Dashboard from "../views/Dashboard";
import CreateCode from "../views/CreateCode";

interface IRoute {
  path: string;
  component: React.FunctionComponent | React.ComponentClass;
  exact: boolean;
  auth?: boolean | null | undefined;
}

interface Routes {
  [key: string]: IRoute;
}

export default {
  home: {
    exact: true,
    component: Home,
    path: "/",
    auth: null,
  },
  languages: {
    exact: true,
    component: Languages,
    path: "/languages",
    auth: null,
  },
  language: {
    exact: true,
    component: Language,
    path: "/languages/:title",
    auth: null,
  },
  createCode: {
    exact: true,
    component: CreateCode,
    path: "/codes/new",
    auth: true,
  },
  code: {
    exact: true,
    component: Code,
    path: "/codes/:id",
    auth: null,
  },
  login: {
    exact: true,
    component: Login,
    path: "/login",
    auth: false,
  },
  register: {
    exact: true,
    component: Register,
    path: "/register",
    auth: false,
  },
  logout: {
    exact: true,
    component: Logout,
    path: "/logout",
    auth: true,
  },
  dashboard: {
    exact: true,
    component: Dashboard,
    path: "/dashboard",
    auth: true,
  },
  "not-found": {
    exact: false,
    component: () => <h1>Not found!</h1>,
    path: "**",
    auth: false,
  },
} as Routes;
