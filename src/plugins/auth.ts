import { gql } from "@apollo/client";
import client from "../api/graphql";
type token = string | null;

export const PersistentAuth = {
  set accessToken(value: token) {
    this.storeToken("accessToken", value);
  },
  get accessToken(): token {
    return localStorage.getItem("accessToken");
  },

  set refreshToken(value: token) {
    this.storeToken("refreshToken", value);
  },
  get refreshToken(): token {
    return localStorage.getItem("refreshToken");
  },

  storeToken(key: string, value: token) {
    if (value === null) localStorage.removeItem(key);
    else localStorage.setItem(key, value);
  },
};

export async function storeUser(
  setUser: React.Dispatch<React.SetStateAction<any | null>>
): Promise<void> {
  const query = gql`
    {
      getMe {
        id
        username
        email
      }
    }
  `;
  try {
    const { data } = await client.query({
      query,
    });
    if (!data.getMe) {
      return setUser(null);
    }
    // getNewAccessToken(setUser);
    return setUser({ ...data.getMe });
  } catch (error) {
    return getNewAccessToken(setUser);
  }
}

export function discardUser(
  setUser: React.Dispatch<React.SetStateAction<any | null>>
) {
  PersistentAuth.accessToken = null;
  PersistentAuth.refreshToken = null;
  return setUser(null);
}

export async function getNewAccessToken(
  setUser: React.Dispatch<React.SetStateAction<any | null>>
) {
  const mutation = gql`
    mutation {
      refreshLogin {
        accessToken
      }
    }
  `;
  try {
    const { data } = await client.mutate({
      mutation,
      context: { useRefreshToken: true },
    });
    PersistentAuth.accessToken = data.refreshLogin.accessToken;
    return storeUser(setUser);
  } catch (error) {
    return discardUser(setUser);
  }
}

export default { PersistentAuth };
