import React from "react";
import { useParams } from "react-router-dom";
import { gql, useQuery } from "@apollo/client";
import CodeList from "../components/CodeList";

export default function Language() {
  const { title } = useParams();
  const query = gql`
    query Language($title: String!) {
      getLanguage(title: $title) {
        id
        title
        description
        codesCount
        codes {
          title
          post {
            id
            createdAt
            author {
              username
            }
            rate
          }
          language {
            title
          }
        }
      }
    }
  `;
  const { loading, error, data } = useQuery(query, {
    variables: {
      title,
    },
  });
  if (loading) return "Loading...";
  if (error) return "Error!";
  return (
    <>
      <h1 className="text-3xl">{data.getLanguage.title} Latest Codes</h1>
      <CodeList codes={data.getLanguage.codes} />
    </>
  );
}
