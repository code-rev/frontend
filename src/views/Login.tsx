import React from "react";
import LoginForm from "../components/LoginForm";

export default function Login() {
  return (
    <div className="grid h-full">
      <div className="md:w-2/3 mx-auto bg-gray-100 p-5 rounded my-auto">
        <h1 className="text-3xl mb-5">Login</h1>
        <LoginForm />
      </div>
    </div>
  );
}
