import React from "react";
import CodeList from "../components/CodeList";
import { gql, useQuery } from "@apollo/client";

export default function Home() {
  const query = gql`
    query Code {
      getCodes {
        title
        post {
          id
          createdAt
          author {
            username
          }
          rate
        }
        language {
          title
        }
      }
    }
  `;
  const { loading, error, data } = useQuery(query);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!!</p>;
  return (
    <>
      <h1 className="text-3xl">Latest codes</h1>
      <CodeList codes={data.getCodes} />
    </>
  );
}
