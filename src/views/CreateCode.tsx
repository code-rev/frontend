import React from "react";
import { CodeForm } from "../components/CodeForm";

export default function CreateCode() {
  return (
    <div>
      <h1 className="text-3xl">Create Code</h1>
      <CodeForm />
    </div>
  );
}
