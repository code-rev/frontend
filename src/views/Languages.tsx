import React from "react";
import { useQuery, gql } from "@apollo/client";
import LanguageList from "../components/LanguageList";

export default function Languages() {
  const query = gql`
    {
      getLanguages {
        id
        title
        description
        codesCount
      }
    }
  `;
  const { loading, error, data } = useQuery(query);
  if (loading) return "Loading...";
  if (error) return "Error!";
  return (
    <>
      <h1 className="text-3xl">Languages</h1>
      <LanguageList languages={data.getLanguages} />
    </>
  );
}
