import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { gql, useQuery } from "@apollo/client";
import CodeItem from "../components/CodeItem";
import { ICode } from "../components/CodeItem";
import { IPost } from "../components/PostItem";

export default function Code() {
  const [code, setCode] = useState<null | ICode>(null);
  const { id } = useParams();
  const query = gql`
    query Code($id: Float!) {
      getCode(id: $id) {
        title
        language {
          title
        }
        post {
          id
          rate
          author {
            username
          }
          content
          createdAt
          updatedAt
          answers {
            id
            content
            createdAt
            updatedAt
            rate
            author {
              username
            }
          }
        }
      }
    }
  `;
  const { loading, error, data } = useQuery(query, {
    variables: {
      id: Number(id),
    },
  });

  const onNewAnswer = (newPost: IPost) => {
    console.log(code);
    const newCodeObject = JSON.parse(JSON.stringify(code))
    console.log(newPost)
    if (!newCodeObject.post.answers) newCodeObject.post.answers = [];
    newCodeObject.post.answers.push(newPost)
    setCode(newCodeObject);
  };
  useEffect(() => {
    if (data) setCode(data.getCode);
  }, [data]);

  if (loading) return "Loading...";
  if (error) return "Error!";
  if (code === null) return "Not found.";

  return <CodeItem code={code as ICode} onNewAnswer={onNewAnswer} />;
}
