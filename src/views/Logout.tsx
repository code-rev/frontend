import React, { useContext, useEffect } from "react";
import { PersistentAuth, discardUser } from "../plugins/auth";
import { Redirect } from "react-router-dom";
import routes from "../router/routes";
import { UserContext } from "../context/UserContext";

export default function Logout() {
  const { setUser } = useContext(UserContext);

  PersistentAuth.accessToken = null;
  PersistentAuth.refreshToken = null;

  useEffect(() => discardUser(setUser));

  return <Redirect to={routes.login.path} />;
}
