import React, { useContext } from "react";
import { UserContext } from "../context/UserContext";
import { Link } from "react-router-dom";
import routes from "../router/routes";

export default function Dashboard() {
  const { user } = useContext(UserContext);
  return (
    <div>
      <h1 className="text-3xl">Welcome {user.username}!</h1>
      <Link to={routes.createCode.path} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Create Code!</Link>
    </div>
  );
}
