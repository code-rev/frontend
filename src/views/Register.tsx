import React from "react";
import RegisterForm from "../components/RegisterForm";

export default function Register() {
  return (
    <div className="grid h-full">
      <div className="md:w-2/3 mx-auto bg-gray-100 p-5 rounded my-auto">
        <h1 className="text-3xl mb-5">Register</h1>
        <RegisterForm />
      </div>
    </div>
  );
}
