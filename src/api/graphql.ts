import { PersistentAuth } from "./../plugins/auth";
import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

const httpLink = createHttpLink({
  uri: "http://127.0.0.1:3000/graphql",
});

const authLink = setContext((_, context) => {
  const token = context.useRefreshToken
    ? PersistentAuth.refreshToken
    : PersistentAuth.accessToken;

  return {
    headers: {
      ...context.headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

export default client;
