import React, { useState, useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import Router from "./router";
import Layout from "./components/Layout";
import { ApolloProvider } from "@apollo/client";
import apolloClient from "./api/graphql";
import { UserContext } from "./context/UserContext";
import { PersistentAuth, storeUser, getNewAccessToken } from "./plugins/auth";

function App() {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState<null | boolean>(null);

  useEffect(() => {
    if (!user) {
      setLoading(true);
      if (PersistentAuth.accessToken) {
        storeUser(setUser).then(() => setLoading(false));
      } else {
        setLoading(false);
      }
    }
  }, [user]);

  useEffect(() => {
    if (user) {
      const interval = setInterval(() => {
        getNewAccessToken(setUser);
      }, 3 * 60 * 1000);
      return () => {
        return clearInterval(interval);
      };
    }
  }, [user]);

  if (loading === true || loading === null) return <p>Loading...</p>;
  return (
    <BrowserRouter>
      <UserContext.Provider value={{ user, setUser }}>
        <ApolloProvider client={apolloClient}>
          <Layout>
            <Router />
          </Layout>
        </ApolloProvider>
      </UserContext.Provider>
    </BrowserRouter>
  );
}

export default App;
